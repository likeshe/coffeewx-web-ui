import request from '@/utils/request'

export function loginByUsername(username, password) {
  const data = {
    username,
    password
  }
  return request({
    url: '/api/auth/login',
    method: 'post',
    data
  })
}

export function logout(token) {
  const data = {
    token
  };
  return request({
    url: '/api/auth/logout',
    method: 'post',
    params:data
  })
}

export function getUserInfo(token) {
  var data = {
    token: token
  }
  return request({
    url: '/api/auth/getUserInfo',
    method: 'post',
    data
  })
}

